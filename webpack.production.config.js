const webpack = require( 'webpack' );
const WebpackStrip = require( 'strip-loader' );
const path = require( 'path' );

// main config file
let config = require( './webpack.config' );

// cdn url static assets
config.output.path = path.resolve( '../production/build/' );

// public path
config.output.publicPath = 'build/';

config.plugins.push(
	new webpack.DefinePlugin( {
		'process.env':
		{
			NODE_ENV: JSON.stringify( 'production' )
		}
	} )
);

config.module.loaders.push( {
	test: /\.js$/,
	loader: WebpackStrip.loader( 'console.log' )
} );

module.exports = config;