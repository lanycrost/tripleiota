const path 			= require( 'path' );
const autoprefixer 	= require( 'autoprefixer' );

const getConfig = function ( root )
{
	return {

		// entry file
		entry:
		[
			'babel-polyfill',
			'./index.jsx'
		],

		// application path
		context: root,

		// output options
		output:
		{
			path: './build/',
			filename: 'bundle.js'
		},

		// resolve modules from src context
		resolve:
		{
			root: root,
			extensions: [ '', '.js', '.jsx' ]
		},

		sassLoader:
		{
			includePaths: [ root ]
		},

		// list of webpack loaders
		module:
		{
			loaders:
			[
				{
					exclude: /(node_modules)/,
					loader: 'babel',
					test: /\.js$/,
					query:
					{
						presets: [ 'es2015' ]
					}
				},
				{
					exclude: /(node_modules)/,
					loader: 'babel',
					test: /\.jsx$/,
					query:
					{
						presets: [ 'es2015', 'react' ]
					}
				},
				{
					exclude: /(node_modules)/,
					test: /\.scss$/,
					loaders:
					[
						'style?singleton', 'css', 'postcss-loader', 'sass'
					]
				},
				{
					test: /\.(png|jpg)$/,
					loader: 'file'
				}
			],

			preLoaders:
			[
				{
					exclude: /(node_modules)/,
					test: /\.(js|jsx)$/,
					loader: 'eslint-loader'
				}
			]
		},

		plugins: [],

		eslint:
		{
			configFile: path.resolve( __dirname, '.eslintrc' )
		},

		/**
		 * Return postcss configuration
		 * @return {Array} Return array of postCss plugins
		 */
		postcss: function ()
		{
			return [ autoprefixer ];
		}
	};
};

// Export configuration object by this root
module.exports = getConfig( path.resolve( __dirname, 'src' ) );

// Allow reuse of this configuration in different paths
module.exports.getConfig = getConfig;
