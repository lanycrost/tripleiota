import React from 'react';
import NavWidget from './widgets/Nav';
import SidebarWidget from './widgets/Sidebar';
import AppContent from '../../component/ui/AppContent';

/**
 * @class Main
 */
class Main extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} Render component
	 */
	render()
	{
		return (
			<SidebarWidget>
				<NavWidget />
				<AppContent>
					{ this.props.children }
				</AppContent>
			</SidebarWidget>
		);
	}
}

Main.propTypes =
{
	children: React.PropTypes.element
};

export default Main;