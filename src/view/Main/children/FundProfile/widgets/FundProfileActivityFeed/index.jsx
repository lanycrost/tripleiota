import React from 'react';
import ActivityFeed from '../../../../../../component/feed/ActivityFeed';
import FundProfileStore from '../../../../../../data/stores/FundProfileStore';
import fundProfileAction from '../../../../../../data/actions/fundProfileAction';

/**
 * @class FundProfileActivityFeed
 */
class FundProfileActivityFeed extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} render
	 */
	render()
	{
		return (
			<div>
				<ActivityFeed
					store={ FundProfileStore }
					actions={ fundProfileAction }
				/>
			</div>
		);
	}
}

export default FundProfileActivityFeed;