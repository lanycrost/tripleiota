import React from 'react';
import { Col, Container, Row } from 'react-grid-system';
import Modal from '../../../../../../component/ui/Modal';
import Button from '../../../../../../component/feed/Button';
import Heading from '../../../../../../component/ui/Heading';
import ChartWrapper from '../../../../../../component/ui/ChartWrapper';
import Checkbox from '../../../../../../component/ui/Checkbox';

/**
 * @class FundModals
 */
class FundModals extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state =
		{
			investIsOpen: false,
			withdrawIsOpen: false,
			rangeVal: 3000,
			investChecked: false,
			withdrawChecked: false
		};
	}

	/**
	 * @return {void}
	 */
	onToggleInvestModal()
	{
		this.setState( {
			investIsOpen: !this.state.investIsOpen
		} );
	}

	/**
	 * @return {void}
	 */
	onToggleWithdraw()
	{
		this.setState( {
			withdrawIsOpen: !this.state.withdrawIsOpen
		} );
	}

	/**
	 * @return {void}
	 */
	onToggleInvestCheckbox()
	{
		this.setState( {
			investChecked: !this.state.investChecked
		} );
	}

	/**
	 * @return {void}
	 */
	onToggleWithdrawCheckbox()
	{
		this.setState( {
			withdrawChecked: !this.state.withdrawChecked
		} );
	}

	/**
	 * @param {Object} event event
	 * @return {void}
	 */
	rangeSlider( event )
	{
		this.setState( {
			rangeVal: event.target.value
		} );
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<div>
				<Button
					style={{ marginTop: '26px' }}
					onClick={ this.onToggleInvestModal.bind( this ) }>
					Invest in Fund
				</Button>
				<Modal
					isOpen={ this.state.investIsOpen }
					onOpen={ this.onToggleInvestModal.bind( this ) }>
					<ChartWrapper>
						<Heading>{ `Invest in Fund ${ this.props.fundId }` }</Heading>
						<h4>Investment Amount</h4>
						<input
							type="range"
							min="1"
							max="10000"
							step="1"
							value={ this.state.rangeVal }
							onChange={ this.rangeSlider.bind( this ) }
						/>
						<input
							type="number"
							min="1"
							max="10000"
							readOnly={ true }
							value={ this.state.rangeVal }
						/>
						<Checkbox
							label="Are You Sure?"
							checked={ this.state.investChecked }
							onClick={ this.onToggleInvestCheckbox.bind( this ) }
						/>
						<Container>
							<Row>
								<Col xs={ 6 }>
									<Button
										disabled={ !this.state.investChecked }
										onClick={ this.onToggleInvestModal.bind( this ) }>
										Save Changes
									</Button>
								</Col>
								<Col xs={ 6 }>
									<Button
										onClick={ this.onToggleInvestModal.bind( this ) }>
										Cancel
									</Button>
								</Col>
							</Row>
						</Container>
					</ChartWrapper>
				</Modal>
				<Button
					style={{
						marginTop: '26px',
						marginBottom: '25px'
					}}
					onClick={ this.onToggleWithdraw.bind( this ) }>
					Withdraw
				</Button>
				<Modal
					isOpen={ this.state.withdrawIsOpen }
					onOpen={ this.onToggleWithdraw.bind( this ) }>
					<ChartWrapper>
						<Heading>{ `Withdraw from Fund ${ this.props.fundId }` }</Heading>
						<h4>Withdraw Amount</h4>
						<input
							type="range"
							min="1"
							max="10000"
							step="1"
							value={ this.state.rangeVal }
							onChange={ this.rangeSlider.bind( this ) }
						/>
						<input
							type="number"
							min="1"
							max="10000"
							readOnly={ true }
							value={ this.state.rangeVal }
						/>
						<Checkbox
							label="Are You Sure?"
							checked={ this.state.withdrawChecked }
							onClick={ this.onToggleWithdrawCheckbox.bind( this ) }
						/>
						<Container>
							<Row>
								<Col xs={ 6 }>
									<Button
										disabled={ !this.state.withdrawChecked }
										onClick={ this.onToggleWithdraw.bind( this ) }>
										Save Changes
									</Button>
								</Col>
								<Col xs={ 6 }>
									<Button
										onClick={ this.onToggleWithdraw.bind( this ) }>
										Cancel
									</Button>
								</Col>
							</Row>
						</Container>
					</ChartWrapper>
				</Modal>
			</div>
		);
	}
}

FundModals.propTypes =
{
	fundId: React.PropTypes.string.isRequired
};

export default FundModals;