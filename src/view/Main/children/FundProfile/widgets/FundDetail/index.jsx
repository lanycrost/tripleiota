import React from 'react';
import { Col, Container, Row } from 'react-grid-system';
import { Chart } from 'react-google-charts';
import ChartWrapper from '../../../../../../component/ui/ChartWrapper';
import Banner from '../../../../../../component/ui/Banner';
import Heading from '../../../../../../component/ui/Heading';
import fundDetailAction from '../../../../../../data/actions/fundDetailAction';
import FundDetailStore from '../../../../../../data/stores/FundDetailStore';
import FollowButton from '../../../../../../component/fund/FollowButton';
import FundModals from '../FundModals';

/**
 * @class FundDetail
 */
class FundDetail extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state =
		{
			fund: {}
		};

		this.getFundDetail = this.getFundDetail.bind( this );
	}

	/**
	 * Set fund data of component
	 * @return {void}
	 */
	getFundDetail()
	{

		this.setState( {
			fund: FundDetailStore.getItem( 'fund' )
		} );
	}

	/**
	 * Component mount handler
	 * @return {void}
	 */
	componentWillMount()
	{

		// add change listener
		FundDetailStore.on( 'change', this.getFundDetail.bind( this ) );

		// find all posts
		fundDetailAction.getFundDetail( +this.props.fundId );
	}

	/**
	 * Component unmount handler
	 * @return {void}
	 */
	componentWillUnmount()
	{
		FundDetailStore.removeListener( 'change', this.getFundDetail );
	}

	/**
	 * Fund Follow event
	 * @return {void}
	 */
	static onFollowToggle()
	{
		fundDetailAction.toggleFollow( +this.props.fundId );
	}

	/**
	 * Render component
	 * @return {XML} render
	 */
	render()
	{
		return (
			<div className="trui-fund-detail">
				<Banner />
				<Container>
					<Row>
						<Col xs={ 6 }>
							<Heading>{ `Fund ${ this.props.fundId }` }</Heading>
							<FollowButton
								onToggle={ FundDetail.onFollowToggle.bind( this ) }
								followCount={ this.state.fund.followCount }
								isFollowed={ this.state.fund.isFollowed }
							/>
						</Col>
						<Col xs={ 6 }>
							<FundModals fundId={ this.props.fundId } />
						</Col>
						<Col xs={ 12 }>
							<ChartWrapper>
								<Chart
									data={ this.state.fund.lineData }
									options={ this.state.fund.lineOptions }
									chartType="LineChart"
									graph_id="LineChart"
									width="100%"
									height="400px"
								/>
								<h4>Performance</h4>
							</ChartWrapper>
						</Col>
						<Col xs={ 12 }>
							<ChartWrapper>
								<Chart
									data={ this.state.fund.barData }
									options={ this.state.fund.barOptions }
									chartType="ColumnChart"
									graph_id="BarChart"
									width="100%"
									height="400px"
								/>
								<h4>Stock Distribution</h4>
							</ChartWrapper>
						</Col>
						<Col xs={ 12 }>
							<ChartWrapper>
								<Chart
									data={ this.state.fund.pieData }
									options={ this.state.fund.pieOptions }
									chartType="PieChart"
									graph_id="PieChart"
									width="100%"
									height="400px"
								/>
								<h4>Stock Distribution</h4>
							</ChartWrapper>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

FundDetail.propTypes =
{
	fundId: React.PropTypes.string.isRequired
};

export default FundDetail;