import React from 'react';
import { Col, Container, Row } from 'react-grid-system';
import FundProfileActivityFeed from './widgets/FundProfileActivityFeed';
import FundDetail from './widgets/FundDetail';

/**
 * @class FundProfile
 * @classdesc FundProfile
 */
class FundProfile extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} render
	 */
	render()
	{
		const fundId = this.props.routeParams.fundId ?
			this.props.routeParams.fundId :
			'1';

		return (
			<Container>
				<Row>
					<Col xs={ 5 }>
						<FundDetail fundId={ fundId } />
					</Col>
					<Col xs={ 7 }>
						<FundProfileActivityFeed />
					</Col>
				</Row>
			</Container>
		);
	}
}

FundProfile.propTypes =
{
	routeParams: React.PropTypes.shape( {
		fundId: React.PropTypes.string.isRequired
	} ).isRequired
};

export default FundProfile;