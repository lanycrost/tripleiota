import FundProfile from './index';

export default {
	path: 'fund-profile/:fundId',
	component: FundProfile
};