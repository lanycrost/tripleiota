import React from 'react';
import FilterContainer from '../../../../../../component/feed/FilterContainer';
import ActivityFeed from '../../../../../../component/feed/ActivityFeed';
import FeedStore from '../../../../../../data/stores/FeedStore';
import feedActions from '../../../../../../data/actions/feedActions';

/**
 * @class HomeActivityFeed
 */
class HomeActivityFeed extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.filters = [
			{
				title: 'Traders',
				alias: 'traders'
			},
			{
				title: 'News',
				alias: 'news'
			},
			{
				title: 'Following',
				alias: 'following'
			},
			{
				title: 'More',
				alias: 'more'
			}
		];

		this.state =
		{
			activeFilter: this.filters[0]
		};

	}

	/**
	 * Filter posts on activity feed
	 * @param {Object} filter Filter object
	 *  @param {String} filter.alias filter alias
	 *  @param {String} filter.title filter title
	 * @return {string} alias
	 */
	onFilter( filter )
	{
		this.setState( {
			activeFilter: filter
		} );
	}

	/**
	 * Render component
	 * @return {XML} render
	 */
	render()
	{
		return (
			<div>
				<FilterContainer
					filters={ this.filters }
					onFilter={ this.onFilter.bind( this ) }
					active={ this.state.activeFilter }
				/>
				<ActivityFeed
					store={ FeedStore }
					actions={ feedActions }
				/>
			</div>
		);
	}
}

export default HomeActivityFeed;