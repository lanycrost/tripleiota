import React from 'react';
import UserReportsStore from '../../../../../../data/stores/UserReportStore';
import reportActions from '../../../../../../data/actions/reportActions';
import { Chart } from 'react-google-charts';
import ChartWrapper from '../../../../../../component/ui/ChartWrapper';
import hashHistory from 'react-router/lib/hashHistory';

/**
 * @class UserReports
 */
class UserReports extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state = {
			reports: []
		};

		this.findAll = this.findAll.bind( this );

		this.chartEvents = [
			{
				eventName : 'select',
				callback  : ( Chart ) =>
				{
					let piePice = Chart.chart.getSelection();

					if ( piePice.length )
					{
						let piePice = Chart.chart.getSelection();

						if ( piePice.length )
						{
							hashHistory.push( `/portfolio/${ piePice[0].row + 1 }` );
						}
					}
				}
			}
		];

	}

	/**
	 * Set report list
	 * @return {void}
	 */
	findAll()
	{
		this.setState( {
			reports: UserReportsStore.getItem( 'reports' )
		} );
	}

	/**
	 * Mount handler
	 * @return {void}
	 */
	componentWillMount()
	{
		UserReportsStore.on( 'change', this.findAll );

		reportActions.findAll();
	}

	/**
	 * Unmount handler
	 * @return {void}
	 */
	componentWillUnmount()
	{
		UserReportsStore.removeListener( 'change', this.findAll );
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<div>

				<h4>Initial Investment: <span>£ 2,500.00</span></h4>
				<h4>Current Worth: <span>£ 3,640.70</span></h4>

				<ChartWrapper>
					<Chart
						data={ this.state.reports.pieData }
						options={ this.state.reports.pieOptions }
						chartType="PieChart"
						graph_id="PieChart"
						width="100%"
						height="400px"
						chartEvents={ this.chartEvents }
					/>
					<h4>Overall +12%</h4>
				</ChartWrapper>
				<ChartWrapper>
					<Chart
						data={ this.state.reports.lineData }
						options={ this.state.reports.lineOptions }
						chartType="LineChart"
						graph_id="LineChart"
						width="100%"
						height="400px"
					/>
				</ChartWrapper>
				<ChartWrapper>
					<Chart
						data={ this.state.reports.barData }
						options={ this.state.reports.barOptions }
						chartType="ComboChart"
						graph_id="ComboChart"
						width="100%"
						height="400px"
					/>
				</ChartWrapper>
			</div>
		);
	}
}

export default UserReports;