import React from 'react';
import Table from '../../../../../../component/ui/Table';
import UserFundStore from '../../../../../../data/stores/UserFundStore';
import fundActions from '../../../../../../data/actions/fundActions';
import Link from 'react-router/lib/Link';

/**
 * @class FundTable
 */
class FundTable extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state =
		{
			funds: [],
			fundId: null
		};

		this.findAll = this.findAll.bind( this );
	}

	/**
	 * Set fund list
	 * @return {void}
	 */
	findAll()
	{
		this.setState( {
			funds: UserFundStore.getItem( 'funds' ),
			fundId: UserFundStore.getItem( 'fundId' )
		} );
	}

	/**
	 * Mount handler
	 * @return {void}
	 */
	componentWillMount()
	{
		UserFundStore.on( 'change', this.findAll );

		fundActions.findAll();
	}

	/**
	 * Unmount handler
	 * @return {void}
	 */
	componentWillUnmount()
	{
		UserFundStore.removeListener( 'change', this.findAll );
	}

	/**
	 * @return {Array} List of rendered funds
	 */
	renderFunds()
	{
		const map = ( item, i ) =>
		{
			return (
				<tr key={ i }>
					<td>
						<Link to={ `/portfolio/${ item.id }` }>
							{ item.name }
						</Link>
					</td>
					<td>{ item.initial }</td>
					<td>{ item.current }</td>
					<td>{ item.change }</td>
					<td>{ item.fees }</td>
				</tr>
			);
		};

		return this.state.funds.map( map );
	}

	/**
	 * Render component
	 * @return {XML} xml content
	 */
	render()
	{
		return (
			<div>
				<Table
					border={ true }
					hover={ true }
					striped={ true }>
					<thead>
						<tr>
							<td>Name</td>
							<td>Initial</td>
							<td>Current</td>
							<td>% change</td>
							<td>Fees</td>
						</tr>
					</thead>
					<tbody>
						{ this.renderFunds() }
					</tbody>
				</Table>
			</div>
		);
	}
}

export default FundTable;