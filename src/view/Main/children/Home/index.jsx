import React from 'react';
import { Col, Container, Row } from 'react-grid-system';
import Heading from '../../../../component/ui/Heading';
import FundTable from './widgets/FundTable';
import ActivityFeed from './widgets/HomeActivityFeed';
import UserReports from './widgets/UserReports';

/**
 * @class Home
 */
class Home extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} render
	 */
	render()
	{
		return (
			<Container>
				<Row>
					<Col xs={ 7 }>
						<Heading>Activity Feed</Heading>
						<ActivityFeed />
					</Col>
					<Col xs={ 5 }>
						<Heading>Portfolio summary</Heading>
						<UserReports />
						<FundTable />
					</Col>
				</Row>
			</Container>
		);
	}
}

export default Home;