import React from 'react';
import CollapsablePanel from './widgets/CollapsablePanel';
import FundSlider from './widgets/FundSlider';
import Panel from '../../../../component/ui/Panel';

/**
 * @class Portfolio
 */
class Portfolio extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		const fundId = this.props.routeParams.fundId === 'all' ?
						'1' :
						this.props.routeParams.fundId;

		return (
			<div>
				<CollapsablePanel />
				<Panel expanded={ true }>
					<FundSlider fundId={ fundId } />
				</Panel>
			</div>
		);
	}
}

Portfolio.propTypes =
{
	routeParams: React.PropTypes.shape( {
		fundId: React.PropTypes.string.isRequired
	} ).isRequired
};

export default Portfolio;