import Home from './index';
import fundActions from '../../../../data/actions/fundActions';

export default {
	path: 'portfolio/:fundId',
	component: Home,
	onEnter: ( nextState ) =>
	{
		const fundId = nextState.params.fundId === 'all' ?
			'1' :
			nextState.params.fundId;

		fundActions.findOne( parseInt( fundId ) );
	}
};