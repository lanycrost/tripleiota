import React from 'react';
import CollapseButton from '../../../../../../component/ui/CollapseButton';
import Panel from '../../../../../../component/ui/Panel';
import AssetsPanel from '../AssetsPanel';

/**
 * @class CollapsablePanel
 */
class CollapsablePanel extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state =
		{
			collapsePanelExpanded: true
		};
	}

	/**
	 * Toggle comment bar in the bottom of post
	 * @return {void}
	 */
	toggleCollapsePanel()
	{
		this.setState( {
			collapsePanelExpanded: !this.state.collapsePanelExpanded
		} );
	}

	/**
	 * Render component
	 * @return {XML} render
	 */
	render()
	{
		const icon = this.state.collapsePanelExpanded ?
						'minus' :
						'plus';

		return (
			<div className="trui-collapsabl-panel">
				<CollapseButton
					icon={ icon }
					onToggle={ this.toggleCollapsePanel.bind( this ) }
				/>
				<Panel expanded={ this.state.collapsePanelExpanded } >
					<AssetsPanel />
				</Panel>
			</div>
		);
	}
}

CollapsablePanel.propTypes =
{
	collapsePanelExpanded: React.PropTypes.bool
};

export default CollapsablePanel;