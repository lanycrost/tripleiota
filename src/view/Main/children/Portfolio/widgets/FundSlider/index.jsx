import React from 'react';
import { Col, Row } from 'react-grid-system';
import Link from 'react-router/lib/Link';
import Heading from '../../../../../../component/ui/Heading';
import ChartWrapper from '../../../../../../component/ui/ChartWrapper';
import Slider from '../../../../../../component/ui/Slider';
import UserReportsStore from '../../../../../../data/stores/UserReportStore';
import UserFundStore from '../../../../../../data/stores/UserFundStore';
import reportActions from '../../../../../../data/actions/reportActions';
import { Chart } from 'react-google-charts';
import fundActions from '../../../../../../data/actions/fundActions';
import hashHistory from 'react-router/lib/hashHistory';

/**
 * @class FundSlider
 */
class FundSlider extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state = {
			reports: [],
			fund: {}
		};

		this.findAll = this.findAll.bind( this );
	}

	/**
	 * Set report list
	 * @return {void}
	 */
	findAll()
	{
		const reports = UserReportsStore.getItem( 'reports' );
		const fund = UserFundStore.getItem( 'fund' );

		this.setState( {
			reports: reports,
			fund: fund
		} );
	}

	/**
	 * Mount handler
	 * @return {void}
	 */
	componentWillMount()
	{
		UserReportsStore.on( 'change', this.findAll );
		UserFundStore.on( 'change', this.findAll );

		reportActions.findAll();
	}

	/**
	 * Unmount handler
	 * @return {void}
	 */
	componentWillUnmount()
	{
		UserReportsStore.removeListener( 'change', this.findAll );
		UserFundStore.removeListener( 'change', this.findAll );
	}

	/**
	 * Slide event
	 * @param {string} type Slide type
	 * @return {void}
	 */
	onSlide( type )
	{
		let fundId = 1;

		if ( type === 'next' )
		{
			fundId = fundActions.getNextId( this.props.fundId );
		}
		else if ( type === 'previous' )
		{
			fundId = fundActions.getPreviousId( this.props.fundId );
		}

		hashHistory.push( `/portfolio/${ fundId }` );
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<Slider onSlide={ this.onSlide.bind( this ) }>
				<Row>
					<Col xs={ 3 }>
						<Heading>
							<Link to={ `/fund-profile/${ this.props.fundId }` }>
								Fund { this.props.fundId }
							</Link>
						</Heading>
						<h4>Initial Investment: <span>{ this.state.fund.initial }</span></h4>
						<h4>Current Worth: <span>{ this.state.fund.current }</span></h4>
					</Col>
					<Col xs={ 5 } >
						<ChartWrapper>
							<Chart
								data={ this.state.fund.data }
								options={ this.state.reports.pieOptions }
								chartType="PieChart"
								graph_id="PieChart-fund"
								width="100%"
								height="300px"
							/>
							<h4>Overall +12%</h4>
						</ChartWrapper>
					</Col>
					<Col xs={ 4 } >
						<ChartWrapper>
							<Chart
								data={ this.state.reports.lineData }
								options={ this.state.reports.lineOptions }
								chartType="LineChart"
								graph_id="LineChart"
								width="100%"
								height="300px"
							/>
						</ChartWrapper>
					</Col>
				</Row>
			</Slider>
		);
	}

}

FundSlider.propTypes =
{
	fundId: React.PropTypes.string
};

export default FundSlider;