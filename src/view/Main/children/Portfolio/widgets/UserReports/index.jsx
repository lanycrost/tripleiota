import React from 'react';
import UserReportsStore from '../../../../../../data/stores/UserReportStore';
import reportActions from '../../../../../../data/actions/reportActions';
import { Col, Row } from 'react-grid-system';
import { Chart } from 'react-google-charts';
import ChartWrapper from '../../../../../../component/ui/ChartWrapper';
import hashHistory from 'react-router/lib/hashHistory';

/**
 * @class UserReports
 */
class UserReports extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state =
		{
			reports: []
		};

		this.findAll = this.findAll.bind( this );

		this.chartEvents = [
			{
				eventName : 'select',
				callback  : ( Chart ) =>
				{
					let piePice = Chart.chart.getSelection();

					if ( piePice.length )
					{
						hashHistory.push( `/portfolio/${ piePice[0].row + 1 }` );
					}
				}
			}
		];
	}

	/**
	 * Set fund list
	 * @return {void}
	 */
	findAll()
	{
		this.setState( {
			reports: UserReportsStore.getItem( 'reports' )
		} );
	}

	/**
	 * Mount handler
	 * @return {void}
	 */
	componentWillMount()
	{
		UserReportsStore.on( 'change', this.findAll );

		reportActions.findAll();
	}

	/**
	 * Unmount handler
	 * @return {void}
	 */
	componentWillUnmount()
	{
		UserReportsStore.removeListener( 'change', this.findAll );
	}

	/**
	 * Render component
	 * @return {XML} render
	 */
	render()
	{
		return (
			<div className="trui-assets-panel">
				<Row>
					<Col xs={ 6 } >
						<ChartWrapper>
							<Chart
								data={ this.state.reports.pieData }
								options={ this.state.reports.pieOptions }
								chartType="PieChart"
								graph_id="PieChart"
								width="100%"
								height="250px"
								chartEvents={ this.chartEvents }
							/>
							<h4>Overall +12%</h4>
						</ChartWrapper>
					</Col>
					<Col xs={ 6 } >
						<Chart
							data={ this.state.reports.barData }
							options={ this.state.reports.barOptions }
							chartType="ComboChart"
							graph_id="ComboChart"
							width="100%"
							height="250px"
						/>
					</Col>
				</Row>
			</div>
		);
	}
}

export default UserReports;