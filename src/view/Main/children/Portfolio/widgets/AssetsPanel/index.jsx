import React from 'react';
import { Col, Container, Row } from 'react-grid-system';
import Heading from '../../../../../../component/ui/Heading';
import FundTable from '../../../Home/widgets/FundTable';
import UserReports from '../UserReports';

/**
 * @class AssetsPanel
 */
class AssetsPanel extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} render
	 */
	render()
	{
		return (
			<div className="trui-assets-panel">
				<Container>
					<Heading>Total Assets</Heading>
					<h4>Initial Investment: <span>£ 2,500.00</span></h4>
					<h4>Current Worth: <span>£ 3,640.70</span></h4>
					<Row>
						<Col xs={ 7 }>
							<UserReports />
						</Col>
						<Col xs={ 5 }>
							<FundTable />
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default AssetsPanel;