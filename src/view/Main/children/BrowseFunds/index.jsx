import React from 'react';
import Heading from '../../../../component/ui/Heading';
import SearchBar from './widgets/SearchBar';
import FundTable from '../Home/widgets/FundTable';
import { Container } from 'react-grid-system';

/**
 * @class BrowseFunds
 * @classdesc BrowseFunds
 */
class BrowseFunds extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} render
	 */
	render()
	{
		return (
			<div>
				<Heading>Browse Funds</Heading>
				<SearchBar />
				<Container>
					<FundTable />
				</Container>
			</div>
		);
	}
}

export default BrowseFunds;