import BrowseFunds from './index';

export default {
	path: 'browse-funds',
	component: BrowseFunds
};