import React from 'react';
import { Col, Container, Row } from 'react-grid-system';
import SearchInput from '../../../../../../component/fund/SearchInput';
import Panel from '../../../../../../component/ui/Panel';
import CollapseButton from '../../../../../../component/ui/CollapseButton';
import Checkbox from '../../../../../../component/ui/Checkbox';

/**
 * @class SearchBar
 * @classdesc SearchBar
 */
class SearchBar extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state =
		{
			collapsePanelExpanded: false
		};
	}

	/**
	 * Toggle comment bar in the bottom of post
	 * @return {void}
	 */
	toggleCollapsePanel()
	{
		this.setState( {
			collapsePanelExpanded: !this.state.collapsePanelExpanded
		} );
	}

	/**
	 * Render component
	 * @return {XML} render
	 */
	render()
	{
		const icon = this.state.collapsePanelExpanded ?
			'minus' :
			'plus';

		return (
			<div>
				<Container>
					<Row>
						<Col xs={ 8 } >
							<SearchInput />
						</Col>
						<Col xs={ 4 } >
							<CollapseButton
								icon={ icon }
								onToggle={ this.toggleCollapsePanel.bind( this ) }
								text="Advanced Search"
							/>
						</Col>
					</Row>
					<Panel expanded={ this.state.collapsePanelExpanded } >
						<Container>
							<Row>
								<Col xs={ 3 } >
									<Checkbox
										label="Option"
										checked={ false }
									/>
									<Checkbox
										label="Option"
										checked={ false }
									/>
								</Col>
								<Col xs={ 3 } >
									<Checkbox
										label="Option"
										checked={ false }
									/>
									<Checkbox
										label="Option"
										checked={ false }
									/>
								</Col>
								<Col xs={ 3 } >
									<Checkbox
										label="Option"
										checked={ false }
									/>
									<Checkbox
										label="Option"
										checked={ false }
									/>
								</Col>
								<Col xs={ 3 } >
									<Checkbox
										label="Option"
										checked={ false }
									/>
									<Checkbox
										label="Option"
										checked={ false }
									/>
								</Col>
							</Row>
						</Container>
					</Panel>
				</Container>
			</div>
		);
	}
}

export default SearchBar;