import React from 'react';
import Sidebar, { Header, MenuItem, MenuTitle } from '../../../../component/ui/Sidebar';
import logo from './images/logo.jpg';

/**
 * @class SidebarWidget
 */
class SidebarWidget extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state = {
			isOpen: false
		};
	}

	/**
	 * Open Sidebar
	 * @return {void}
	 */
	openSidebar()
	{
		this.setState( {
			isOpen: true
		} );
	}

	/**
	 * Close Sidebar
	 * @return {void}
	 */
	closeSidebar()
	{
		this.setState( {
			isOpen: false
		} );
	}

	/**
	 * Render Menu Content
	 * @return {XML} - menu Content
	 */
	getMenuContent()
	{

		return (
			<div
				onMouseOver={ this.openSidebar.bind( this ) }
				onMouseOut={ this.closeSidebar.bind( this ) }>
				<Header
					sidebarIsOpen={ this.state.isOpen }
					imageSrc={ logo }>
					Triple Iota
				</Header>
				<MenuItem
					title="Home Page"
					icon="desktop"
					href="/home"
					sidebarIsOpen={ this.state.isOpen }
					hasChild={ false }
					subMenus={ [] }
				/>
				<MenuItem
					title="My Portfolio"
					icon="pie-chart"
					href="/portfolio/all"
					sidebarIsOpen={ this.state.isOpen }
					hasChild={ false }
					subMenus={ [] }
				/>
				<MenuItem
					title="Messages"
					icon="envelope-o"
					href="/messages"
					sidebarIsOpen={ this.state.isOpen }
					hasChild={ false }
					subMenus={ [] }
				/>
				<MenuItem
					title="Browse Funds"
					icon="list"
					href="/browse-funds"
					sidebarIsOpen={ this.state.isOpen }
					hasChild={ false }
					subMenus={ [] }
				/>
				<MenuItem
					title="News Feed"
					icon="desktop"
					href="/news-feed"
					sidebarIsOpen={ this.state.isOpen }
					hasChild={ false }
					subMenus={ [] }
				/>
				<MenuTitle
					sidebarIsOpen={ this.state.isOpen }
					title="My Funds"
				/>
				<MenuItem
					title="Mutual Founds"
					icon="bookmark"
					href="/mutual-funds"
					sidebarIsOpen={ this.state.isOpen }
					hasChild={ false }
					subMenus={ [
						{
							title: "Mutual Found 1",
							href: "mutual-1"
						},
						{
							title: "Mutual Found 2",
							href: "mutual-2"
						},
						{
							title: "Mutual Found 3",
							href: "mutual-3"
						}
					] }
				/>
				<MenuItem
					title="ET Funds"
					icon="bookmark"
					href="/mutual-funds"
					sidebarIsOpen={ this.state.isOpen }
					hasChild={ false }
					subMenus={ [
						{
							title: "ET Funds 1",
							href: "et-funds-1"
						},
						{
							title: "ET Funds 1",
							href: "et-funds-2"
						},
						{
							title: "ET Funds 3",
							href: "et-funds-3"
						}
					] }
				/>
				<MenuItem
					title="Hedge Founds"
					icon="bookmark"
					href="/hedge-funds"
					sidebarIsOpen={ this.state.isOpen }
					hasChild={ false }
					subMenus={ [
						{
							title: "Hedge Founds 1",
							href: "hedge-1"
						},
						{
							title: "Hedge Founds 2",
							href: "hedge-2"
						},
						{
							title: "Hedge Founds 3",
							href: "hedge-3"
						}
					] }
				/>
				<MenuItem
					title="Crowd Funds"
					icon="bookmark"
					href="/crowd-funds"
					sidebarIsOpen={ this.state.isOpen }
					hasChild={ false }
					subMenus={ [
						{
							title: "Crowd Funds 1",
							href: "crowd-1"
						},
						{
							title: "Crowd Funds 2",
							href: "crowd-2"
						},
						{
							title: "Crowd Funds 3",
							href: "crowd-3"
						}
					] }
				/>
			</div>
		);
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		const menuContent = this.getMenuContent();

		return (
			<Sidebar
				menuContent={ menuContent }
				isOpen={ this.state.isOpen }>
				{ this.props.children }
			</Sidebar>
		);
	}
}

SidebarWidget.propTypes =
{
	children: React.PropTypes.node.isRequired
};

export default SidebarWidget;