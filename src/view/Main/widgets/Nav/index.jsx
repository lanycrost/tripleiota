import React from 'react';
import NavBar, { Nav, NavItem, FormGroup } from '../../../../component/ui/NavBar';
import FormControl from '../../../../component/ui/FormControl';
import ProfileImage from '../../../../component/ui/ProfileImage';
import { Col } from 'react-grid-system';

/**
 * @class NavWidget
 */
class NavWidget extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state = {};
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<div>
				<NavBar>
					<Nav side="left">
						<FormGroup href="#">
							<FormControl
								type="text"
								placeholder="Search Triple Iota"
							/>
						</FormGroup>
					</Nav>
					<Nav side="right">
						<NavItem href="#">
							<Col xs={ 4 }>
								<ProfileImage />
							</Col>
							<Col xs={ 8 }>
								Tim <span className="bold">Wu</span>
							</Col>
						</NavItem>
						<NavItem href="#">
							<i className="fa fa-cog fa-lg" />
						</NavItem>
						<NavItem href="#">
							<i className="fa fa-comments fa-lg" />
						</NavItem>
					</Nav>
				</NavBar>
			</div>
		);
	}
}

export default NavWidget;