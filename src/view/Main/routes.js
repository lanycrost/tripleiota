import Main from './index';

// child routes
import home from './children/Home/routes';
import portfolio from './children/Portfolio/routes';
import fundProfile from './children/FundProfile/routes';
import browseFunds from './children/BrowseFunds/routes';

export default {

	childRoutes:
	[
		{
			path: '/',
			component: Main,
			childRoutes:
			[
				home,
				portfolio,
				fundProfile
				// browseFunds
			],

			indexRoute:
			{
				onEnter( nextState, replace )
				{
					replace( '/home' );
				}
			}
		}
	]
};