export default {

	/**
	 * @constant FIND_ALL
	 */
	FIND_ALL: 'FUND_PROFILE_FEED_FIND_ALL',

	/**
	 * @constant NEXT_PAGE
	 */
	NEXT_PAGE: 'FUND_PROFILE_FEED_NEXT_PAGE',

	/**
	 * @constant ADD_COMMENT
	 */
	ADD_COMMENT: 'FUND_PROFILE_FEED_ADD_COMMENT',

	/**
	 * @constant LIKE
	 */
	LIKE: 'FUND_PROFILE_FEED_LIKE'
};