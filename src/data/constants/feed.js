export default {

	/**
	 * @constant FIND_ALL
	 */
	FIND_ALL: 'FEED_FIND_ALL',

	/**
	 * @constant NEXT_PAGE
	 */
	NEXT_PAGE: 'FEED_NEXT_PAGE',

	/**
	 * @constant ADD_COMMENT
	 */
	ADD_COMMENT: 'FEED_ADD_COMMENT',

	/**
	 * @constant LIKE
	 */
	LIKE: 'FEED_LIKE'
};