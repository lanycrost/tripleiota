export default {

	/**
	 * @constant FIND_ALL
	 */
	FIND_ALL: 'FUND_FUND_FIND_ALL',

	/**
	 * @constant FIND_ONE
	 */
	FIND_ONE: 'FUND_FIND_ONE'
};