export default {

	/**
	 * @constant FIND_ALL
	 */
	FIND_ALL: 'REPORT_FIND_ALL',

	FIND_ONE: 'REPORT_FIND_ONE'
};