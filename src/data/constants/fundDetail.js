export default {

	/**
	 * @constant GET_FUND_DETAIL
	 */
	GET_FUND_DETAIL: 'FUND_DETAIL_GET_FUND_DETAIL',

	/**
	 * @constant GET_FUND_DETAIL
	 */
	FOLLOW: 'FUND_DETAIL_FOLLOW'

};