import BaseStore from './BaseStore';
import Dispatcher from '../Dispatcher';
import feed from '../constants/feed';

/**
 * @class FeedStore
 * @classdesc Manages list of activity feed posts
 */
class FeedStore extends BaseStore
{

	/**
	 * @constructor
	 * @param {Object} Dispatcher Dispatcher instance
	 */
	constructor( Dispatcher )
	{
		super( Dispatcher );

		this.addActionHandler( feed.FIND_ALL, this.setPosts.bind( this ) )
			.addActionHandler( feed.NEXT_PAGE, this.prependPosts.bind( this ) )
			.addActionHandler( feed.ADD_COMMENT, this.addComment.bind( this ) )
			.addActionHandler( feed.LIKE, this.like.bind( this ) );
	}

	/**
	 * Add comment to post
	 * @param {Object} data container
	 *  @param {Object} data.post Post instance
	 *  @param {Object} data.newComment new comment Instance
	 * @return {void}
	 */
	addComment( data )
	{
		let posts = this.getItem( 'posts' );

		// given post index
		const postIndex = posts.indexOf( data.post );

		// change single post
		posts[ postIndex ].comments = posts[ postIndex ].comments || [];
		posts[ postIndex ].comments.unshift( data.newComment );

		this.setItem( 'posts', posts );
		this.emit( 'change' );
	}

	/**
	 * Update like count on post
	 * @param {Object} data container
	 *  @param {Object} data.post post instance
	 *  @param {Object} data.likeCount like count
	 *  @param {Object} data.isLiked isLiked by user or not
	 * @return {void}
	 */
	like( data )
	{
		let posts = this.getItem( 'posts' );

		// given post index
		const postIndex = posts.indexOf( data.post );

		// change post
		posts[ postIndex ].likeCount = data.likeCount;
		posts[ postIndex ].isLiked = data.isLiked;

		this.setItem( 'posts', posts );
		this.emit( 'change' );
	}

	/**
	 * Prepend posts in container
	 * @param {Object} data container
	 *  @param {Array} data.posts List of posts
	 * @return {void}
	 */
	prependPosts( data )
	{
		const posts = this.getItem( 'posts' );

		this.setItem( 'posts', [].concat( posts, data.posts ) );

		this.emit( 'change' );
	}

	/**
	 * Set posts in container
	 * @param {Object} data container
	 *  @param {Array} data.posts List of posts
	 * @return {void}
	 */
	setPosts( data )
	{
		this.setItem( 'posts', data.posts );
		this.emit( 'change' );
	}
}

export default new FeedStore( Dispatcher );