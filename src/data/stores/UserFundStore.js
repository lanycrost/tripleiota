import BaseStore from './BaseStore';
import Dispatcher from '../Dispatcher';
import fund from '../constants/fund';

/**
 * @class UserFundStore
 */
class UserFundStore extends BaseStore
{

	/**
	 * @constructor
	 * @param {Dispatcher} Dispatcher Dispatcher instance
	 */
	constructor( Dispatcher )
	{
		super( Dispatcher );

		this.addActionHandler( fund.FIND_ONE, this.setFund.bind( this ) )
			.addActionHandler( fund.FIND_ALL, this.setFunds.bind( this ) );

	}

	/**
	 * @param {Object} data data container
	 *  @param {Array} data.fund single fund object
	 * @return {void}
	 */
	setFund( data )
	{
		this.setItem( 'fund', data.fund );
		this.emit( 'change' );
	}

	/**
	 * Save funds and emit change
	 * @param {Object} data data container
	 *  @param {Array} data.funds list of funds
	 * @return {void}
	 */
	setFunds( data )
	{
		this.setItem( 'funds', data.funds );
		this.emit( 'change' );
	}
}

export default new UserFundStore( Dispatcher );