import events from 'events';

/**
 * @class BaseStore
 */
class BaseStore extends events.EventEmitter
{

	/**
	 *
	 * @constructor
	 * @param {Object} Dispatcher Dispatcher instance
	 */
	constructor( Dispatcher )
	{
		super();

		/**
		* Container for storing actions
		* @type {Array}
		*/
		this.actionHandler = [];

		/**
		 * Data container
		 * @type {{}}
		 */
		this.container = {};

		Dispatcher.register( this.dispatcherHandler.bind( this ) );
	}

	/**
	 * Dispatcher callback handler: Finds correct action with handler and calls it
	 * If action has multiple handlers each handler should be called.
	 * @param {Object} dispatchMessage - Message interface
	 *  @param {String} dispatchMessage.action - Action to dispatch
	 *  @param {Object} dispatchMessage.data - Data to pass to stores
	 * @return {void}
	 */
	dispatcherHandler( dispatchMessage )
	{
		let action = dispatchMessage.action;

		if ( !action )
		{
			return;
		}

		for ( let i = 0; i < this.actionHandler.length; i++ )
		{
			const actionInstance = this.actionHandler[ i ];

			if ( actionInstance.name === action )
			{
				if ( typeof actionInstance.handler !== 'function' )
				{
					throw new Error( "Handler is Not a Function" );
				}

				actionInstance.handler( dispatchMessage.data || null );
			}
		}
	}

	/**
	 * Create action listener
	 * @param {string} actionName - Action name
	 * @param {function} actionHandler - Action Handler
	 * @return {BaseStore} - return this class object
	 */
	addActionHandler( actionName, actionHandler )
	{
		this.actionHandler.push( {
			'name': actionName,
			'handler': actionHandler
		} );

		return this;
	}

	/**
	 * Get item from container
	 * @param {String} itemName item name to get
	 * @return {*} data
	 */
	getItem( itemName )
	{
		return this.container[ itemName ];
	}

	/**
	 * Set item in container
	 * @param {String} itemName itemName to set
	 * @param {*} data data to set
	 * @return {void}
	 */
	setItem( itemName, data )
	{
		this.container[ itemName ] = data;
	}
}

export default BaseStore;