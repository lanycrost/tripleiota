import BaseStore from './BaseStore';
import Dispatcher from '../Dispatcher';
import report from '../constants/report';

/**
 * @class UserReportsStore
 * @classdesc Manages User Reports Data
 */
class UserReportsStore extends BaseStore
{

	/**
	 * @constructor
	 * @param {object} Dispatcher - Dispatcher instance
	 */
	constructor( Dispatcher )
	{
		super( Dispatcher );

		this.addActionHandler( report.FIND_ALL, this.setReports.bind( this ) )
			.addActionHandler( report.FIND_ONE, this.getItem.bind( this ) );
	}

	/**
	 * Save reports and emit change
	 * @param {Object} data data container
	 *  @param {Array} data.funds list of funds
	 * @return {void}
	 */
	setReports( data )
	{
		this.setItem( 'reports', data.reports );
		this.emit( 'change' );
	}
}

export default new UserReportsStore( Dispatcher );