import BaseStore from './BaseStore';
import Dispatcher from '../Dispatcher';
import fundDetail from '../constants/fundDetail';

/**
 * @class FundDetailStore
 */
class FundDetailStore extends BaseStore
{

	/**
	 * @constructor
	 * @param {Object} Dispatcher Dispatcher instance
	 */
	constructor( Dispatcher )
	{
		super( Dispatcher );

		this.addActionHandler( fundDetail.GET_FUND_DETAIL, this.setDetails.bind( this ) )
			.addActionHandler( fundDetail.FOLLOW, this.follow.bind( this ) );
	}

	/**
	 * Update like count on post
	 * @param {Object} data container
	 *  @param {Object} data.followCount follow count
	 *  @param {Object} data.isFollowed isFollowed by user or not
	 * @return {void}
	 */
	follow( data )
	{
		let fund = this.getItem( 'fund' );

		// change post
		fund.followCount = data.fund.followCount;
		fund.isFollowed = data.fund.isFollowed;

		this.setItem( 'fund', fund );
		this.emit( 'change' );
	}

	/**
	 * Set posts in container
	 * @param {Object} data container
	 *  @param {Array} data.posts List of posts
	 * @return {void}
	 */
	setDetails( data )
	{
		this.setItem( 'fund', data.fund );
		this.emit( 'change' );
	}
}

export default new FundDetailStore( Dispatcher );