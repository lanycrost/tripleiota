import feed from '../constants/feed';
import Dispatcher from '../Dispatcher';

export default {

	/**
	 * Find all posts
	 * @return {void}
	 */
	findAll()
	{
		Dispatcher.dispatch( {
			action: feed.FIND_ALL,
			data: {
				posts: [
					{
						text: 'News story with details on it etc coming in from some news outlet or posted by user',
						isLiked: false,
						commentCount: 21,
						shareCount: 3,
						likeCount: 100,
						isAd: false,
						comments: [
							{
								text: 'Testing comments section'
							}
						]
					},
					{
						text: 'Trending Amazing fund Advertisiment',
						isAd: true
					},
					{
						text: `
							Fund manager post about the fund that you're
							invested in. Informing you on what he's doing and why etc.`,
						likeCount: 120,
						commentCount: 41,
						shareCount: 5,
						isLiked: false,
						isAd: false,
						comments: [
							{
								text: 'How are you?'
							}
						]
					},
					{
						text: 'Recently viewed fund',
						isAd: true
					}
				]
			}
		} );
	},

	/**
	 * Paginate activity feed
	 * @return {void}
	 */
	nextPage()
	{
		Dispatcher.dispatch( {
			action: feed.NEXT_PAGE,
			data: {
				posts: [
					{
						text: 'Hello',
						isLiked: false,
						commentCount: 5,
						shareCount: 15,
						likeCount: 5,
						isAd: false,
						comments: [
							{
								text: 'Hello this is comment'
							}
						]
					}
				]
			}
		} );
	},

	/**
	 * Add new comment on post
	 * @param {Object} post post instance
	 * @param {String} newComment new comment text
	 * @return {void}
	 */
	addComment( post, newComment )
	{
		Dispatcher.dispatch( {
			action: feed.ADD_COMMENT,
			data: {
				post: post,
				newComment: {
					text: newComment
				}
			}
		} );
	},

	/**
	 * Update likeCount and isLiked on post
	 * @param {Object} post post instance
	 * @return {void}
	 */
	toggleLike( post )
	{
		const isLiked = !post.isLiked;
		const likeCount = post.isLiked ?
							post.likeCount - 1 :
							post.likeCount + 1;

		Dispatcher.dispatch( {
			action: feed.LIKE,
			data: {
				post: post,
				likeCount: likeCount,
				isLiked: isLiked
			}
		} );
	}
};