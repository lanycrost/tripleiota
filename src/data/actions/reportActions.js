import Dispatcher from '../Dispatcher';
import report from '../constants/report';
import db from '../../../db/reports';

export default {

	findAll()
	{
		Dispatcher.dispatch( {
			action: report.FIND_ALL,
			data: {
				reports: {
					lineData: db.lineData,
					lineOptions: db.lineOptions,
					barData: db.barData,
					barOptions: db.barOptions,
					pieData: db.pieData,
					pieOptions: db.pieOptions
				}
			}
		} );
	}
};