import fundProfile from '../constants/fundProfile';
import Dispatcher from '../Dispatcher';

export default {

	/**
	 * Find all posts
	 * @return {void}
	 */
	findAll()
	{
		Dispatcher.dispatch( {
			action: fundProfile.FIND_ALL,
			data: {
				posts: [
					{
						text: 'Description of fund. Introduction to team. Selling fund to investors',
						isAd: true
					},
					{
						text: 'Fund manager post above the fund that you re invested in informing you on what he\'s doing and why etc.',
						likeCount: 120,
						isAd: false,
						commentCount: 41,
						shareCount: 5,
						isLiked: false,
						comments: [
							{
								text: 'How are you?'
							}
						]
					},
					{
						text: 'Trending Amazing fund Advertisiment',
						isAd: true
					},
					{
						text: `
							Fund manager post about the fund that you're
							invested in. Informing you on what he's doing and why etc.`,
						likeCount: 120,
						commentCount: 41,
						shareCount: 5,
						isLiked: false,
						isAd: false,
						comments: [
							{
								text: 'How are you?'
							}
						]
					},
					{
						text: 'Recently viewed fund',
						isAd: true
					}
				]
			}
		} );
	},

	/**
	 * Paginate activity fundProfile
	 * @return {void}
	 */
	nextPage()
	{
		Dispatcher.dispatch( {
			action: fundProfile.NEXT_PAGE,
			data: {
				posts: [
					{
						text: 'Hey How are you',
						isLiked: false,
						commentCount: 5,
						shareCount: 15,
						likeCount: 5,
						isAd: false,
						comments: [
							{
								text: 'Hello this is comment :D'
							}
						]
					},
					{
						text: 'Recently viewed fund',
						isAd: true
					}
				]
			}
		} );
	},

	/**
	 * Add new comment on post
	 * @param {Object} post post instance
	 * @param {String} newComment new comment text
	 * @return {void}
	 */
	addComment( post, newComment )
	{
		Dispatcher.dispatch( {
			action: fundProfile.ADD_COMMENT,
			data: {
				post: post,
				newComment: {
					text: newComment
				}
			}
		} );
	},

	/**
	 * Update likeCount and isLiked on post
	 * @param {Object} post post instance
	 * @return {void}
	 */
	toggleLike( post )
	{
		const isLiked = !post.isLiked;
		const likeCount = post.isLiked ?
		post.likeCount - 1 :
		post.likeCount + 1;

		Dispatcher.dispatch( {
			action: fundProfile.LIKE,
			data: {
				post: post,
				likeCount: likeCount,
				isLiked: isLiked
			}
		} );
	}
};