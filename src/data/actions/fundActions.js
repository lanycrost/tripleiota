import Dispatcher from '../Dispatcher';
import fundConstants from '../constants/fund';

export default {

	funds: [
		{
			id: 1,
			name: 'Fund 1',
			initial: '$100',
			current: '$134',
			change: '+10%',
			fees: '20/2',
			data:
			[
				[ 'Option',			'Value' ],
				[ 'Asset 1 +24%',  	120		],
				[ 'Asset 2 +2%',  	100		],
				[ 'Asset 3 +7%',  	10		],
				[ 'Asset 4 +14%',  	22		],
				[ 'Asset 5 +5%',  	80		],
				[ 'Asset 6 +19%',  	10		],
				[ 'Asset 7 +13%',  	24		]
			]
		},
		{
			id: 2,
			name: 'Fund 2',
			initial: '$250',
			current: '$343',
			change: '+12%',
			fees: '20/2',
			data:
			[
				[ 'Option',			'Value' ],
				[ 'Asset 1 +24%',  	24		],
				[ 'Asset 2 +2%',  	22		],
				[ 'Asset 3 +7%',  	10		],
				[ 'Asset 4 +14%',  	100		],
				[ 'Asset 5 +5%',  	80		],
				[ 'Asset 6 +19%',  	10		],
				[ 'Asset 7 +13%',  	120		]
			]
		},
		{
			id: 3,
			name: 'Fund 3',
			initial: '$1000',
			current: '$1304',
			change: '-3%',
			fees: '20/2',
			data:
			[
				[ 'Option',			'Value' ],
				[ 'Asset 1 +24%',  	120		],
				[ 'Asset 2 +2%',  	22		],
				[ 'Asset 3 +7%',  	10		],
				[ 'Asset 4 +14%',  	100		],
				[ 'Asset 5 +5%',  	80		],
				[ 'Asset 6 +19%',  	10		],
				[ 'Asset 7 +13%',  	24		]
			]
		}
	],

	getNextId( currentId )
	{
		const FUNDS = this.funds;
		let result = FUNDS[0].id;

		for ( let i = 1; i < FUNDS.length; i++ )
		{
			if ( FUNDS[ i ].id > currentId )
			{
				result = FUNDS[i].id;
			}
		}

		for ( let i = 1; i < FUNDS.length; i++ )
		{
			if ( FUNDS[ i ].id < result && FUNDS[ i ].id > currentId )
			{
				result = FUNDS[ i ].id;
			}
		}

		return result;
	},

	getPreviousId( currentId )
	{
		const FUNDS = this.funds;
		let result = FUNDS[0].id;

		for ( let i = 1; i < FUNDS.length; i++ )
		{
			if ( FUNDS[ i ].id > result && FUNDS[ i ].id < currentId )
			{
				result = FUNDS[i].id;
			}
		}

		return result;
	},

	findAll()
	{
		Dispatcher.dispatch( {
			action: fundConstants.FIND_ALL,
			data:
			{
				funds: this.funds
			}
		} );
	},

	/**
	 * Find single fund with given id
	 * @param {Int} fundId fund id to find
	 * @return {void}
	 */
	findOne( fundId )
	{
		let fund = null;
		let item = null;

		for ( item of this.funds )
		{
			if ( item.id === fundId )
			{
				fund = item;
			}
		}

		Dispatcher.dispatch( {
			action: fundConstants.FIND_ONE,
			data:
			{
				fund: fund
			}
		} );
	}

};