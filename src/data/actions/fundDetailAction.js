import Dispatcher from '../Dispatcher';
import fundDetailConstants from '../constants/fundDetail';
import db from '../../../db/reports';

export default {

	funds: [
		{
			id: 1,
			isFollowed: false,
			followCount: 154,
			lineData: db.lineData,
			lineOptions: db.lineOptions,
			barData: db.barData,
			barOptions: db.barOptions,
			pieOptions: db.pieOptions,
			pieData:
			[
				[ 'Option',			'Value' ],
				[ 'Asset 1 +24%',  	120		],
				[ 'Asset 2 +2%',  	100		],
				[ 'Asset 3 +7%',  	10		],
				[ 'Asset 4 +14%',  	22		],
				[ 'Asset 5 +5%',  	80		],
				[ 'Asset 6 +19%',  	10		],
				[ 'Asset 7 +13%',  	24		]
			]
		},
		{
			id: 2,
			isFollowed: false,
			followCount: 256,
			lineData: db.lineData,
			lineOptions: db.lineOptions,
			barData: db.barData,
			barOptions: db.barOptions,
			pieOptions: db.pieOptions,
			pieData:
			[
				[ 'Option',			'Value' ],
				[ 'Asset 1 +24%',  	24		],
				[ 'Asset 2 +2%',  	22		],
				[ 'Asset 3 +7%',  	10		],
				[ 'Asset 4 +14%',  	100		],
				[ 'Asset 5 +5%',  	80		],
				[ 'Asset 6 +19%',  	10		],
				[ 'Asset 7 +13%',  	120		]
			]
		},
		{
			id: 3,
			isFollowed: false,
			followCount: 44,
			lineData: db.lineData,
			lineOptions: db.lineOptions,
			barData: db.barData,
			barOptions: db.barOptions,
			pieOptions: db.pieOptions,
			pieData:
			[
				[ 'Option',			'Value' ],
				[ 'Asset 1 +24%',  	120		],
				[ 'Asset 2 +2%',  	22		],
				[ 'Asset 3 +7%',  	10		],
				[ 'Asset 4 +14%',  	100		],
				[ 'Asset 5 +5%',  	80		],
				[ 'Asset 6 +19%',  	10		],
				[ 'Asset 7 +13%',  	24		]
			]
		}
	],

	toggleFollow( fundId )
	{
		let data = {};

		for ( let i = 0; i < this.funds.length; i++ )
		{
			if ( this.funds[ i ].id === fundId )
			{
				data = this.funds[ i ];
			}
		}

		data.isFollowed = !data.isFollowed;
		data.followCount = data.isFollowed ?
				data.followCount - 1 :
				data.followCount + 1;

		Dispatcher.dispatch( {
			action: fundDetailConstants.FOLLOW,
			data: {
				fund: data
			}
		} );
	},

	getFundDetail( fundId )
	{
		let data = {};

		for ( let i = 0; i < this.funds.length; i++ )
		{
			if ( this.funds[ i ].id === fundId )
			{
				data = this.funds[ i ];
			}
		}

		Dispatcher.dispatch( {
			action: fundDetailConstants.GET_FUND_DETAIL,
			data:
			{
				fund: data
			}
		} );
	}
};