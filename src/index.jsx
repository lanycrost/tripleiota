import React from 'react';
import ReactDOM from 'react-dom';
import { Router, hashHistory } from 'react-router';
import MainRoutes from './view/Main/routes';
import './lib-css/normalize.scss';

ReactDOM.render(
	React.createElement( Router, {
		history: hashHistory,
		routes: MainRoutes,
		onUpdate: () =>
		{
			window.scrollTo( 0, 0 );
		}
	} ),
	document.getElementById( 'react-root' ) );