import axios from 'axios';

/**
 * @class Http
 */
class Http
{

	/**
	 * @constructor
	 */
	constructor()
	{

		/**
		 * Configuration container
		 * @type {Object}
		 */
		this.config = {};
	}

	/**
	 * Initialize configuration object
	 * @param {Object} config Configuration object
	 *  @param {String} config.baseUrl Base url to prepend on every request url
	 *  @param {String} config.defaultParams default query paramters
	 *  @param {function} config.onResponse function that receives json response and executes code on every request
	 * @returns {void}
	 */
	configure( config )
	{
		this.config = config;
	}

	/**
	 * Modifies given configuration object
	 * @param {Object} config Current request configuration
	 * @return {Object} Returns configured request configuration
	 */
	getConfig( config )
	{

		// prepend baseUrl to path
		config.url = this.config.baseUrl + config.url;

		// assign default query params to given request params
		config.params = Object.assign( {}, this.config.defaultParams, config.params );

		// transform response method
		// TODO: inject config object instead of binding to class
		config.transformResponse = this.transformResponse.bind( this );

		return config;
	}

	/**
	 * Parse row response string to JSON data
	 * @param {String} rawData Raw response to transform
	 * @return {Object} return parsed data
	 * @static
	 */
	transformResponse( rawData )
	{
		try
		{
			let parsedData = JSON.parse( rawData );

			if ( typeof this.config.onResponse === 'function' )
			{
				this.config.onResponse( parsedData );
			}
		}
		catch ( err )
		{
			throw new Error( err );
		}
	}

	/**
	 * Perform GET request
	 * @param {String} url request url
	 * @param {Object} config request config
	 * @return {axios.Promise} promise object
	 */
	get( url, config )
	{
		config.url = url;
		config.method = 'GET';

		return axios( this.getConfig( config ) );
	}

	/**
	 * Perform POST request
	 * @param {String} url request url
	 * @param {Object} data request body
	 * @param {Object} config request config
	 * @return {axios.Promise} promise object
	 */
	post( url, data, config )
	{
		config.url = url;
		config.data = data;
		config.method = 'POST';

		return axios( this.getConfig( config ) );
	}

	/**
	 * Perform PUT request
	 * @param {String} url request url
	 * @param {Object} data request body
	 * @param {Object} config request config
	 * @return {axios.Promise} promise object
	 */
	put( url, data, config )
	{
		config.url = url;
		config.data = data;
		config.method = 'PUT';

		return axios( this.getConfig( config ) );
	}

	/**
	 * Perform DELETE request
	 * @param {String} url request url
	 * @param {Object} config request config
	 * @return {axios.Promise} promise object
	 */
	delete( url, config )
	{
		config.url = url;
		config.method = 'DELETE';

		return axios( this.getConfig( config ) );
	}
}

export default Http;