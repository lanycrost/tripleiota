import React from 'react';
import Tabs from '../../ui/Tabs';
import './component.scss';

/**
 * @classdesc Contains filters with bootstrap tabs
 * @class FilterContainer
 */
class FilterContainer extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<div className="trui-filter-container">
				<Tabs
					onFilter={ this.props.onFilter }
					items={ this.props.filters }
					active={ this.props.active }
				/>
				<div className="clear" />
			</div>
		);
	}
}

FilterContainer.propTypes = {
	filters: React.PropTypes.arrayOf(
		React.PropTypes.shape( {
			title: React.PropTypes.string.isRequired,
			alias: React.PropTypes.string.isRequired
		} )
	).isRequired,
	active: React.PropTypes.shape( {
		title: React.PropTypes.string.isRequired,
		alias: React.PropTypes.string.isRequired
	} ),
	onFilter: React.PropTypes.func.isRequired
};

export default FilterContainer;