import React from 'react';
import './component.scss';

/**
 * @class Button
 */
class Button extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<div
				className="trui-feed-button"
				{ ...this.props } >
				{ this.props.children }
			</div>
		);
	}
}

Button.propTypes =
{
	children: React.PropTypes.node
};

export default Button;