import React from 'react';
import PostTextContainer from '../PostTextContainer';
import LikeButton from '../LikeButton';
import CommentButton from '../CommentButton';
import ShareButton from '../ShareButton';
import CommentBar from '../CommentBar';
import { Col, Row } from 'react-grid-system';
import './component.scss';

/**
 * @class PostContainer
 */
class PostContainer extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state =
		{
			commentBarExpanded: false
		};
	}

	/**
	 * Toggle like action
	 * @return {void}
	 */
	onLikeToggle()
	{
		this.props.onLikeToggle();
	}

	/**
	 * Added comment action
	 * @param {String} comment New comment which will be created
	 * @return {void}
	 */
	onComment( comment )
	{
		this.props.onComment( comment );
	}

	/**
	 * Toggle comment bar in the bottom of post
	 * @return {void}
	 */
	toggleCommentBar()
	{
		this.setState( {
			commentBarExpanded: !this.state.commentBarExpanded
		} );
	}

	/**
	 * Get Button Row if post is not AD
	 * @return {XML} xml
	 */
	getButtonRow()
	{
		if ( !this.props.post.isAd )
		{
			return (
				<div>
					<Row>
						<Col xs={ 4 }>
							<LikeButton
								likeCount={ this.props.post.likeCount }
								isLiked={ this.props.post.isLiked }
								onToggle={ this.onLikeToggle.bind( this ) }
							/>
						</Col>
						<Col xs={ 4 }>
							<CommentButton
								onClick={ this.toggleCommentBar.bind( this ) }
								commentCount={ this.props.post.commentCount }
							/>
						</Col>
						<Col xs={ 4 }>
							<ShareButton shareCount={ this.props.post.shareCount } />
						</Col>
					</Row>
					<CommentBar
						comments={ this.props.post.comments }
						expanded={ this.state.commentBarExpanded }
						onComment={ this.onComment.bind( this ) }
					/>
				</div>
			);
		}

		return null;
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		const bottomRow = this.getButtonRow();

		return (
			<div className="trui-post-container">
				<PostTextContainer text={ this.props.post.text } />
				{ bottomRow }
			</div>
		);
	}
}

PostContainer.propTypes =
{
	post: React.PropTypes.shape( {
		text: React.PropTypes.string,
		isLiked: React.PropTypes.bool,
		commentCount: React.PropTypes.number,
		shareCount: React.PropTypes.number,
		likeCount: React.PropTypes.number,
		isAd: React.PropTypes.bool,
		comments: React.PropTypes.arrayOf(
			React.PropTypes.shape( {
				text: React.PropTypes.string
			} )
		)
	} ).isRequired,
	onLikeToggle: React.PropTypes.func.isRequired,
	onComment: React.PropTypes.func.isRequired
};

export default PostContainer;