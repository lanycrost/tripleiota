import React from 'react';
import Panel from '../../ui/Panel';
import FormControl from '../../ui/FormControl';
import CommentRow from '../CommentRow';
import './component.scss';

/**
 * @class CommentBar
 */
class CommentBar extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state =
		{
			comment: ''
		};
	}

	/**
	 * Keypress event on comment input
	 * @param {Object} event Event instance
	 * @return {void}
	 */
	onKeyPress( event )
	{
		if ( event.charCode === 13 )
		{
			this.props.onComment( this.state.comment );

			this.setState( {
				comment: ''
			} );
		}
	}

	/**
	 * Change event handler
	 * @param {Object} event Event instance
	 * @return {void}
	 */
	onChange( event )
	{
		this.setState( {
			comment: event.target.value
		} );
	}

	/**
	 * Render comments
	 * @return {Array|null} List of comments
	 */
	renderComments()
	{
		if ( typeof this.props.comments === 'undefined' )
		{
			return null;
		}

		const map = ( item, i ) =>
		{
			return (
				<CommentRow
					comment={ item }
					key={ i }
				/>
			);
		};

		return this.props.comments.map( map );
	}

	/**
	 * Render component
	 * @returns {XML} render
	 */
	render()
	{
		return (
			<div className="trui-comment-bar">
				<Panel
					expanded={ this.props.expanded } >
					<div>
						<div className="comment-container">
							{ this.renderComments() }
						</div>

						<div className="trui-comment-input">
							<FormControl
								placeholder="Type a comment"
								value={ this.state.comment }
								onKeyPress={ this.onKeyPress.bind( this ) }
								onChange={ this.onChange.bind( this ) }
							/>
						</div>

					</div>
				</Panel>
			</div>
		);
	}

}

CommentBar.propTypes =
{
	expanded: React.PropTypes.bool,
	comments: React.PropTypes.arrayOf(
		React.PropTypes.shape( {
			text: React.PropTypes.string
		} )
	),
	onComment: React.PropTypes.func
};

export default CommentBar;