import React from 'react';
import Button from '../Button';

/**
 * @class ShareButton
 */
class ShareButton extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state = {};
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<div className="trui-share-button">
				<Button
					onClick={ this.props.onClick } >
					<i className="fa fa-share" />
					<span>{ this.props.shareCount }</span> shares
				</Button>
			</div>
		);
	}
}

ShareButton.propTypes =
{
	shareCount: React.PropTypes.number.isRequired,
	onClick: React.PropTypes.func
};

export default ShareButton;