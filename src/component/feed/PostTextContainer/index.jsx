import React from 'react';
import './component.scss';

/**
 * @class PostTextContainer
 * @classdesc Contains post text
 */
class PostTextContainer extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<section className="trui-post-text-container">
				{this.props.text}
			</section>
		);
	}
}

PostTextContainer.propTypes =
{
	text: React.PropTypes.string.isRequired
};

export default PostTextContainer;