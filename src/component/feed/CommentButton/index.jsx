import React from 'react';
import Button from '../Button';

/**
 * @class CommentButton
 */
class CommentButton extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state = {};
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<div className="trui-comment-button">
				<Button
					onClick={ this.props.onClick } >
					<i className="fa fa-list-alt" />
					<span>{ this.props.commentCount }</span> comments
				</Button>
			</div>
		);
	}
}

CommentButton.propTypes =
{
	commentCount: React.PropTypes.number.isRequired,
	onClick: React.PropTypes.func
};

export default CommentButton;