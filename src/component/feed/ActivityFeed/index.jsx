import React from 'react';
import PostContainer from '../PostContainer';
import InfiniteScroll from '../../ui/InifiniteScroll';

/**
 * @class ActivityFeed
 */
class ActivityFeed extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state =
		{
			posts: [],
			store: {},
			actions: {}
		};

		this.findAll = this.findAll.bind( this );
	}

	/**
	 * Set posts of component
	 * @return {void}
	 */
	findAll()
	{
		this.setState( {
			posts: this.props.store.getItem( 'posts' )
		} );
	}

	/**
	 * Component mount handler
	 * @return {void}
	 */
	componentWillMount()
	{

		// add change listener
		this.props.store.on( 'change', this.findAll );

		// find all posts
		this.props.actions.findAll();

		this.setState( {
			store: this.props.store,
			actions: this.props.actions
		} );
	}

	/**
	 * Component unmount handler
	 * @return {void}
	 */
	componentWillUnmount()
	{
		this.state.store.removeListener( 'change', this.findAll );
	}

	/**
	 * Contact new posts for infinite scroll
	 * @static
	 * @return {void}
	 */
	static onInfiniteScroll()
	{
		this.state.actions.nextPage();
	}

	/**
	 * Post add comment event
	 * @param {Object} item post instance
	 * @param {String} newComment new comment text
	 * @return {void}
	 */
	static onComment( item, newComment )
	{
		this.state.actions.addComment( item, newComment );
	}

	/**
	 * Post like event
	 * @param {Object} item Post instance
	 * @return {void}
	 */
	static onLikeToggle( item )
	{
		this.state.actions.toggleLike( item );
	}

	/**
	 * Render posts to DOM
	 * @return {Array} Return array of rendered DOM elements
	 */
	renderPosts()
	{
		const map = ( item, i ) =>
		{
			return (
				<PostContainer
					onComment={ ActivityFeed.onComment.bind( this, item ) }
					onLikeToggle={ ActivityFeed.onLikeToggle.bind( this, item ) }
					post={ item }
					key={ i }
				/>
			);
		};

		return this.state.posts.map( map );
	}

	/**
	 * Render component
	 * @return {XML} render
	 */
	render()
	{
		return (
			<div>
				<InfiniteScroll
					onInfiniteScroll={ ActivityFeed.onInfiniteScroll.bind( this ) }
					hasMore={ true } >
					{ this.renderPosts() }
				</InfiniteScroll>
			</div>
		);
	}
}

ActivityFeed.propTypes =
{
	store: React.PropTypes.shape( {
		getItem: React.PropTypes.func,
		on: React.PropTypes.func
	} ).isRequired,
	actions: React.PropTypes.shape( {
		addComment: React.PropTypes.func.isRequired,
		findAll: React.PropTypes.func.isRequired,
		nextPage: React.PropTypes.func.isRequired,
		toggleLike: React.PropTypes.func.isRequired
	} ).isRequired
};

export default ActivityFeed;