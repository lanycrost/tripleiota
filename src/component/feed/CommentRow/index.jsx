import React from 'react';
import ProfileImage from '../../ui/ProfileImage';
import { Col, Row } from 'react-grid-system';
import './component.scss';

/**
 * @class CommentRow
 */
class CommentRow extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @returns {XML} render
	 */
	render()
	{
		return (
			<div className="trui-comment-row">
				<Row>
					<Col xs={ 1 }>
						<ProfileImage />
					</Col>
					<Col xs={ 11 }>
						{ this.props.comment.text }
					</Col>
				</Row>
			</div>
		);
	}
}

CommentRow.propTypes =
{
	comment: React.PropTypes.shape( {
		text: React.PropTypes.string
	} )
};

export default CommentRow;