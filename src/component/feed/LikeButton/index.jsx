import React from 'react';
import Button from '../Button';

/**
 * @class LikeButton
 */
class LikeButton extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state = {};
	}

	/**
	 * Toggle button state
	 * @return {void}
	 */
	toggleButton()
	{
		if ( typeof this.props.onToggle === 'function' )
		{
			this.props.onToggle();
		}
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		const color = this.props.isLiked ?
						'#217dbb' :
						'';

		const icon = this.props.isLiked ?
						'fa fa-thumbs-up' :
						'fa fa-thumbs-o-up';

		return (
			<div className="trui-like-button">
				<Button
					style={{ color: color }}
					onClick={ this.toggleButton.bind( this ) } >
					<i className={ icon } />
					{ this.props.likeCount } likes
				</Button>
			</div>
		);
	}
}

LikeButton.propTypes =
{
	likeCount: React.PropTypes.number.isRequired,
	isLiked: React.PropTypes.bool.isRequired,
	onToggle: React.PropTypes.func.isRequired
};

export default LikeButton;