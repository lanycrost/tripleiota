import React from 'react';
import TabItem from '../TabItem';
import './component.scss';

/**
 * @class Tabs
 * @classdesc Renders tab container. Will take care of rendering tabItems with props
 */
class Tabs extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state = {
			itemsStatus: []
		};
	}

	/**
	 * Check is active
	 * @param {Object} item lol
 	 * @return {boolean} ads
	 */
	isActive( item )
	{
		return item === this.props.active;
	}

	/**
	 * Get Tab Items
	 * @return {XML} xml
	 */
	getItems()
	{
		const map = ( item, i ) =>
		{
			return (
				<TabItem
					onFilter={ this.props.onFilter.bind( this, item ) }
					item={ item }
					key={ i }
					isActive={ this.isActive( item ) }
				/>
			);
		};

		return this.props.items.map( map );
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		const tabs = this.getItems();

		return (
			<div className="trui-tabs-wrapper">
				<ul className="trui-tabs">
					{ tabs }
				</ul>
			</div>
		);
	}
}

Tabs.propTypes = {
	items: React.PropTypes.arrayOf( React.PropTypes.shape( {
		title: React.PropTypes.string.isRequired,
		alias: React.PropTypes.string.isRequired
	} ) ).isRequired,
	active: React.PropTypes.shape( {
		title: React.PropTypes.string.isRequired,
		alias: React.PropTypes.string.isRequired
	} ),
	onFilter: React.PropTypes.func
};

export default Tabs;