import React from 'react';
import './component.scss';

/**
 * @class TabItem
 * @classdesc Render single tab
 */
class TabItem extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * @return {void}
	 */
	onToggleActive()
	{
		this.props.onFilter();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{

		return (
			<li
				onClick={ this.onToggleActive.bind( this ) }
				className={ `trui-tab-item ${ this.props.isActive ?
				"active" :
				"" }` }>
				<a>{ this.props.item.title }</a>
			</li>
		);
	}
}

TabItem.propTypes = {
	item: React.PropTypes.shape( {
		title: React.PropTypes.string.isRequired,
		alias: React.PropTypes.string.isRequired,
		isActive: React.PropTypes.bool
	} ).isRequired,
	isActive: React.PropTypes.bool,
	onFilter: React.PropTypes.func
};

export default TabItem;