import React from 'react';
import './component.scss';

/**
 * @class Table
 * @classdesc Renders HTML table element
 */
class Table extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		const bordered = this.props.border ?
			" table-bordered " :
			"";
		const hover = this.props.hover ?
			" table-hover " :
			"";
		const striped = this.props.striped ?
			" table-striped " :
			"";

		return (
			<div className="trui-table">
				<table className={ `${ bordered + hover + striped }` }>
					{ this.props.children }
				</table>
			</div>
		);
	}
}

Table.propTypes =
{
	border: React.PropTypes.bool,
	hover: React.PropTypes.bool,
	striped: React.PropTypes.bool,
	children: React.PropTypes.node.isRequired
};

export default Table;