import React from 'react';
import './component.scss';

/**
 * @classdesc Component For Infinity Scroll
 * @class InfiniteScroll
 */
class InfiniteScroll extends React.Component
{

	/**
	 * @constructor
	 * @param {object} props - Compoennt Propertis
	 */
	constructor( props )
	{
		super( props );
		this.scrollListener = this.scrollListener.bind( this );
	}

	/**
	 * @return {void}
	 */
	componentDidMount()
	{
		this.attachScrollListener();
	}

	/**
	 * @return {void}
	 */
	componentWillUnmount()
	{
		this.detachScrollListener();
	}

	/**
	 * @return {void}
	 */
	scrollListener()
	{
		const container = this._element;

		let scrollTop = window.pageYOffset ?
						window.pageYOffset :
						( document.documentElement || document.body.parentNode || document.body ).scrollTop;
		let position = InfiniteScroll.topPosition( container ) + container.offsetHeight - scrollTop - window.innerHeight;

		if ( position < 250 )
		{
			this.props.onInfiniteScroll();

			if ( !this.props.hasMore )
			{
				this.detachScrollListener();
			}
		}
	}

	/**
	 * @param {object} element - Element
	 * @return {number} - Element top Position
	 */
	static topPosition( element )
	{
		let box = element.getBoundingClientRect();

		const body = document.body;
		const docEl = document.documentElement;

		let scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;

		let clientTop = docEl.clientTop || body.clientTop || 0;

		let top = box.top + scrollTop - clientTop;

		return Math.round( top );
	}

	/**
	 * @return {void}
	 */
	attachScrollListener()
	{
		window.addEventListener( 'scroll', this.scrollListener );
		window.addEventListener( 'resize', this.scrollListener );

		this.scrollListener();
	}

	/**
	 * @return {void}
	 */
	detachScrollListener()
	{
		window.removeEventListener( 'scroll', this.scrollListener );
		window.removeEventListener( 'resize', this.scrollListener );
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<div
				className="trui-infinite-scroll"
				ref={ ( el ) =>
				{
					if ( el )
					{
						this._element = el;
					}
				} } >
				{this.props.children}
			</div>
		);
	}
}

InfiniteScroll.propTypes = {
	children: React.PropTypes.node.isRequired,
	onInfiniteScroll: React.PropTypes.func.isRequired,
	hasMore: React.PropTypes.bool.isRequired
};

export default InfiniteScroll;