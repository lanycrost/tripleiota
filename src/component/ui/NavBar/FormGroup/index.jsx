import React from 'react';
import './component.scss';

/**
 * @class FormGroup
 */
class FormGroup extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XMl} xml
	 */
	render()
	{
		return (
			<div className="trui-navbar-formgroup">
				{ this.props.children }
			</div>
		);
	}
}

FormGroup.propTypes =
{
	children: React.PropTypes.node
};

export default FormGroup;