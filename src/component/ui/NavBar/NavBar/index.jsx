import React from 'react';
import './component.scss';

/**
 * @class NavBar
 * @classdesc Renders full-width container with fixed height: Implemented following bootstrap-style
 */
class NavBar extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{

		return (
			<div className="trui-nav-bar">
				<div className="nav-bar-outlet">
					{ this.props.children }
				</div>
			</div>
		);
	}

}

NavBar.propTypes =
{
	children: React.PropTypes.node.isRequired
};

export default NavBar;