import React from 'react';
import './component.scss';

/**
 * @class Nav
 */
class Nav extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<nav className={ `trui-navbar-nav ${ this.props.side }` }>
				{ this.props.children }
			</nav>
		);
	}
}

Nav.propTypes =
{
	children: React.PropTypes.node,
	side: React.PropTypes.string.isRequired
};

export default Nav;