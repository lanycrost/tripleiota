import React from 'react';
import './component.scss';

/**
 * @class
 */
class NavItem extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<a
				href={ this.props.href }
				className="trui-navbar-navitem" >
				{ this.props.children }
			</a>
		);
	}
}

NavItem.propTypes =
{
	href: React.PropTypes.string.isRequired,
	children: React.PropTypes.node
};

export default NavItem;