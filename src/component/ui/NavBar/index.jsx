import NavBar from './NavBar';
import Nav from './Nav';
import NavItem from './NavItem';
import FormGroup from './FormGroup';

/**
 * export all subcomponents
 */
export {
	Nav,
	NavItem,
	FormGroup
};

/**
 * default will be NavBar
 */
export default NavBar;