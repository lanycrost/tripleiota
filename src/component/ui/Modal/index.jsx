import React from 'react';
import './component.scss';

/**
 * @class Modal
 */
class Modal extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		const open = this.props.isOpen ?
			'open' :
			'';

		return (
			<div className={ `trui-modal-wrapper ${ open }` }>
				<div className="trui-modal-outlet">
					<div className="trui-modal">
						<i
							className="close fa fa-times"
							onClick={ this.props.onOpen }
						/>
						{ this.props.children }
					</div>
				</div>
			</div>
		);
	}
}

Modal.propTypes =
{
	children: React.PropTypes.node.isRequired,
	isOpen: React.PropTypes.bool,
	onOpen: React.PropTypes.func
};

export default Modal;