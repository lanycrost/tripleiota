import React from 'react';
import './component.scss';

/**
 * @class FormControl
 * @classdesc Renders HTML input element
 */
class FormControl extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<input
				className="trui-form-control"
				{ ...this.props }
			/>
		);
	}
}

export default FormControl;