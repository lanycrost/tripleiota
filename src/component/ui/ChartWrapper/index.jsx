import React from 'react';
import './component.scss';

/**
 * @class ChartWrapper
 * @classdesc Wrapper for charts
 */
class ChartWrapper extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @returns {XML} xml
	 */
	render()
	{
		return (
			<div className="trui-chart-wrapper">
				{ this.props.children }
			</div>
		);
	}
}

ChartWrapper.propTypes =
{
	children: React.PropTypes.node
};

export default ChartWrapper;