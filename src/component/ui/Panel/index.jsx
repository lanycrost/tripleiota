import React from 'react';
import './component.scss';

/**
 * @class Panel
 */
class Panel extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		const expandedClass = this.props.expanded ?
								'expanded' :
								'';

		return (
			<div className={ `trui-panel ${ expandedClass } ` } >
				<div className="trui-panel-body">
					{ this.props.children }
				</div>
			</div>
		);
	}
}

Panel.propTypes =
{
	expanded: React.PropTypes.bool,
	children: React.PropTypes.node
};

export default Panel;