import React from 'react';
import Link from 'react-router/lib/Link';
import './component.scss';

/**
 * @class SubMenu
 */
class SubMenu extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render sub menu list
	 * @return {Array} List of submenus
	 */
	renderSubMenus()
	{
		const map = ( item, i ) =>
		{
			return (
				<Link
					activeClassName="active"
					to={ item.href }
					key={ i } >
					<span>{ item.title }</span>
				</Link>
			);
		};

		return this.props.subMenus.map( map );
	}

	/**
	 * Render sub menu
	 * @return {XML|null} sub-menu content
	 */
	render()
	{
		const collapsedClass = this.props.isCollapsed ?
			'collapsed' :
			'';

		return (
			this.props.subMenus.length ?
				<div className={ `trui-sidebar-submenu ${ collapsedClass }` }>{ this.renderSubMenus() }</div> :
				<div />
		);
	}
}

SubMenu.propTypes =
{
	isCollapsed: React.PropTypes.bool,
	subMenus: React.PropTypes.arrayOf( React.PropTypes.shape( {
		href: React.PropTypes.string.isRequired,
		title: React.PropTypes.string.isRequired
	} ) )
};

export default SubMenu;