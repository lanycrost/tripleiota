import Sidebar from './Sidebar';
import Header from './Header';
import MenuItem from './MenuItem';
import SubMenu from './SubMenu';
import MenuTitle from './MenuTitle';

/**
 * default will be Sidebar
 */
export default Sidebar;

/**
 * export all subcomponents
 */
export {
	Header,
	MenuItem,
	SubMenu,
	MenuTitle
};