import React from 'react';
import './component.scss';

/**
 * @class Header
 */
class Header extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<div
				className={ `trui-sidebar-header ${ this.props.sidebarIsOpen ?
				"open" :
				"" }` }>
				<a href="#">
					<div
						style={{ backgroundImage: `url(${ this.props.imageSrc })` }}
						className="icon"
					/>
					<span className="logo">
						{ this.props.children }
					</span>
				</a>
			</div>
		);
	}
}

Header.propTypes =
{
	sidebarIsOpen: React.PropTypes.bool.isRequired,
	imageSrc: React.PropTypes.string.isRequired,
	children: React.PropTypes.node.isRequired
};

export default Header;