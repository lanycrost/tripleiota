import React from 'react';
import './component.scss';

/**
 * @class Sidebar
 * @classdesc Renders container which will include sidebar-menu and content can be div for now. This component contains it’s own subcomponents which are exported from index.js file
 */
class Sidebar extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<div
				className={ `trui-sidebar ${ this.props.isOpen ?
				"open" :
				""
				}` } >
				<div
					className="trui-sidebar-menu">
					{ this.props.menuContent }
				</div>
				<div
					className="trui-sidebar-content" >
					{ this.props.children }
				</div>
			</div>
		);
	}
}

Sidebar.propTypes =
{
	children: React.PropTypes.node.isRequired,
	menuContent: React.PropTypes.node.isRequired,
	isOpen: React.PropTypes.bool.isRequired
};

export default Sidebar;