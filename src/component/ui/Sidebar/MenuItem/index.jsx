import React from 'react';
import Link from 'react-router/lib/Link';
import SubMenu from '../SubMenu';
import './component.scss';

/**
 * @class MenuItem
 */
class MenuItem extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state =
		{
			subMenuCollasped: true,
			sidebarIsOpen: false
		};
	}

	/**
	 * Invoked before a mounted component receives new props
	 * @param {Object} nextProps - receives props
	 * @return {void}
     */
	componentWillReceiveProps( nextProps )
	{
		this.setState( {
			sidebarIsOpen: nextProps.sidebarIsOpen
		} );
	}

	/**
	 * Toggle submenu and icon
	 * @param {Object} event Event instance
	 * @return {void}
	 */
	collapseSubMenu( event )
	{
		event.preventDefault();

		this.setState( {
			subMenuCollasped: !this.state.subMenuCollasped
		} );
	}

	/**
	 * Render toggle icon content
	 * @return {XML|null} toggle icon
	 */
	getToggleIcon()
	{
		const toggleSide = this.state.subMenuCollasped ?
							'left' :
							'down';

		return this.props.subMenus.length ?
			<span
				className="toggle"
				onClick={ this.collapseSubMenu.bind( this ) }>
				<i className={ `fa fa-angle-${ toggleSide }` } />
			</span> :
			null;
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<div
				className={ `trui-sidebar-menu-item ${
				this.state.sidebarIsOpen ?
					"open" :
					""
				}` }>
				<Link
					to={ this.props.href }
					activeClassName="active" >
					<span className="icon">
						<i className={ `fa fa-${ this.props.icon }` } />
					</span>
					{ this.props.title }
					<span>
						{ this.getToggleIcon() }
					</span>
				</Link>
				<div
					className="true-sub-menu-wrapper">
					<SubMenu
						subMenus={ this.props.subMenus }
						isCollapsed={ this.state.subMenuCollasped }
					/>
				</div>
			</div>
		);
	}

}

MenuItem.propTypes =
{
	title: React.PropTypes.string.isRequired,
	icon: React.PropTypes.string.isRequired,
	href: React.PropTypes.string.isRequired,
	hasChild: React.PropTypes.bool,
	sidebarIsOpen: React.PropTypes.bool,
	subMenus: React.PropTypes.arrayOf( React.PropTypes.shape( {
		href: React.PropTypes.string.isRequired,
		title: React.PropTypes.string.isRequired
	} ) )
};

export default MenuItem;