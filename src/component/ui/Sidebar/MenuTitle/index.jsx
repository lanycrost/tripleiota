import React from 'react';
import './component.scss';

/**
 * @class MenuTitle
 */
class MenuTitle extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state = {
			sidebarIsOpen: false
		};
	}

	/**
	 * Invoked before a mounted component receives new props
	 * @param {Object} nextProps - receives props
	 * @return {void}
	 */
	componentWillReceiveProps( nextProps )
	{
		this.setState( {
			sidebarIsOpen: nextProps.sidebarIsOpen
		} );
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<h5
				className={ `trui-sidebar-menu-title ${ this.state.sidebarIsOpen ?
				"open" :
				"" }` }>
				{ this.props.title }
			</h5>
		);
	}
}

MenuTitle.propTypes =
{
	sidebarIsOpen: React.PropTypes.bool.isRequired,
	title: React.PropTypes.string.isRequired
};

export default MenuTitle;