import React from 'react';
import './component.scss';

/**
 * @class AppContent
 * @classdesc Renders poor component with padding for application content
 */
class AppContent extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{

		return (
			<main className="trui-app-content">
				{ this.props.children }
			</main>
		);
	}

}

AppContent.propTypes =
{
	children: React.PropTypes.node.isRequired
};

export default AppContent;