import React from 'react';
import './component.scss';

/**
 * @class Checkbox
 */
class Checkbox extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<div className="trui-input-checkbox">
				<input
					type="checkbox"
					readOnly={ true }
					checked={ this.props.checked }
					onClick={ this.props.onClick }
				/>
				<h4 onClick={ this.props.onClick } >{ this.props.label }</h4>
			</div>
		);
	}
}

Checkbox.propTypes =
{
	label: React.PropTypes.string.isRequired,
	checked: React.PropTypes.bool.isRequired,
	onClick: React.PropTypes.func
};

export default Checkbox;