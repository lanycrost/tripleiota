import React from 'react';
import { Container, Row, Col } from 'react-grid-system';
import './component.scss';

/**
 * @class Slider
 */
class Slider extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Icon click event
	 * @param {string} type event type
	 * @return {void}
	 */
	onSlide( type )
	{
		this.props.onSlide( type );
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<div className="trui-slider">
				<Container>
					<Row>
						<Col xs={ 1 }>
							<div className="trui-slide-icon">
								<i
									onClick={ this.onSlide.bind( this, 'previous' ) }
									className="fa fa-arrow-left"
								/>
							</div>
						</Col>
						<Col xs={ 10 }>
							{ this.props.children }
						</Col>
						<Col xs={ 1 }>
							<div className="trui-slide-icon">
								<i
									onClick={ this.onSlide.bind( this, 'next' ) }
									className="fa fa-arrow-right"
								/>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

Slider.propTypes =
{
	children: React.PropTypes.node,
	onSlide: React.PropTypes.func.isRequired
};

export default Slider;