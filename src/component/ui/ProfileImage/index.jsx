import React from 'react';
import './component.scss';

/**
 * @class ProfileImage
 */
class ProfileImage extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{

		return (
			<div className="trui-profile-image" />
		);
	}
}

ProfileImage.propTypes =
{
	isNav: React.PropTypes.bool,
	url: React.PropTypes.string
};

export default ProfileImage;