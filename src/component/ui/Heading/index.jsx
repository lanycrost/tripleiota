import React from 'react';
import './component.scss';

/**
 * @class Heading
 */
class Heading extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<h1 className="trui-heading">{ this.props.children }</h1>
		);
	}
}

Heading.propTypes =
{
	children: React.PropTypes.node
};

export default Heading;