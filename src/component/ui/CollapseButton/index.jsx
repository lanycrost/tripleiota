import React from 'react';
import './component.scss';

/**
 * @class CollapseButton
 */
class CollapseButton extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		let text = this.props.text ?
			<h4>{ this.props.text }</h4> :
			'';

		return (
			<div className="trui-collapse-button">
				{ text }
				<i
					className={ `fa fa-${ this.props.icon }` }
					onClick={ this.props.onToggle }
				/>
			</div>
		);
	}
}

CollapseButton.propTypes =
{
	icon: React.PropTypes.string.isRequired,
	onToggle: React.PropTypes.func.isRequired,
	text: React.PropTypes.string
};

export default CollapseButton;