import React from 'react';
import './component.scss';
import bannerImg from './images/19.jpg';

/**
 * @class Panel
 */
class Banner extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		return (
			<div
				className="trui-banner"
				style={{ backgroundImage: `url( ${ bannerImg } )` }}
			/>
		);
	}
}

export default Banner;