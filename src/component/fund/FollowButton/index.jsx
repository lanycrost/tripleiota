import React from 'react';
import Button from '../../feed/Button';

/**
 * @class FollowButton
 */
class FollowButton extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();

		this.state = {};
	}

	/**
	 * Toggle button state
	 * @return {void}
	 */
	toggleButton()
	{
		if ( typeof this.props.onToggle === 'function' )
		{
			this.props.onToggle();
		}
	}

	/**
	 * Render component
	 * @return {XML} xml
	 */
	render()
	{
		const buttonText = this.props.isFollowed ?
			'Unfollow' :
			'Follow';

		const color = this.props.isFollowed ?
			'#217dbb' :
			'';

		return (
			<div className="trui-follow-button">
				<Button
					style={{ color: color }}
					onClick={ this.toggleButton.bind( this ) } >
					{ `${ this.props.followCount } ${ buttonText }` }
				</Button>
			</div>
		);
	}
}

FollowButton.propTypes =
{
	followCount: React.PropTypes.number.isRequired,
	isFollowed: React.PropTypes.bool.isRequired,
	onToggle: React.PropTypes.func.isRequired
};

export default FollowButton;