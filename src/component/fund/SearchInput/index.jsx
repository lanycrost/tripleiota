import React from 'react';
import './component.scss';

/**
 * @class SearchBar
 * @classdesc SearchBar
 */
class SearchBar extends React.Component
{

	/**
	 * @constructor
	 */
	constructor()
	{
		super();
	}

	/**
	 * Render component
	 * @return {XML} render
	 */
	render()
	{
		return (
			<div className="trui-search-input">
				<input
					type="text"
					placeholder="Search Funds"
				/>
				<span>
					<button>
						<i className="fa fa-search" />
					</button>
				</span>
			</div>
		);
	}
}

export default SearchBar;