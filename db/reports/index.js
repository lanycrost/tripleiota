let getLineData = function()
{
	let limit = 10000;
	let y = 0;
	let dataPoints = [ [ 'Option', 'Value' ] ];

	for ( let i = 0; i < limit; i += 1 )
	{
		y += Math.random() * 10;
		y -= 5;
		dataPoints.push( [ `${ i }`, y ] );
	}

	return dataPoints;
};

module.exports =
{
	lineData: getLineData(),
	lineOptions: {
		chartArea: {
			left: '2%',
			top: '0',
			width: "96%",
			height: "90%"
		},
		backgroundColor: 'transparent',

		// vAxis: {
		// 	textPosition: 'none',
		// 	baselineColor: 'transparent',
		// 	gridlines: {
		// 		color: 'transparent'
		// 	}
		// },
		// hAxis: {
		// 	textPosition: 'none',
		// 	baselineColor: 'transparent',
		// 	gridlines: {
		// 		color: 'transparent'
		// 	}
		// },
		legend: {
			position: 'none'
		}
	},
	barData: [
		[ 'Month', 'Reserve Assets', 'Other Investment', 'Financial Derivatives', 'Portfolio Investment', 'Direct Investment', 'Financial Account' ],
		[ '2004/05', 165, 938, 522, 998, 450, 614.6 ],
		[ '2005/06', 135, 1120, 599, 1268, 288, 682 ],
		[ '2006/07', 157, 1167, 587, 807, 397, 623 ],
		[ '2007/08', 139, 1110, 615, 968, 215, 609.4 ],
		[ '2008/09', -136, -691, -629, -1026, -366, -569.6 ],
		[ '2009/09', 160, 691, 629, 1026, 366, 569.6 ],
		[ '2010/09', 136, 691, 629, 1026, 366, 569.6 ],
		[ '2011/09', -1365, 691, 629, -1026, 366, 569.6 ],
		[ '2012/09', 136, 691, 629, 1026, 366, 569.6 ],
		[ '2013/09', 136, 691, 629, 1026, 366, 569.6 ],
		[ '2014/09', 136, 691, 629, 1026, 366, 569.6 ],
		[ '2015/09', 136, 691, 629, 1026, 366, 569.6 ],
		[ '2016/09', 136, 691, 629, 1026, 366, 569.6 ],
		[ '2016/10', 136, 691, 629, 1026, 366, 569.6 ],
		[ '2016/11', 136, 691, 629, 1026, 366, 569.6 ]
	],
	barOptions: {
		seriesType: 'bars',
		isStacked: true,
		series: {
			5: {
				type: 'line'
			}
		},
		chartArea: {
			left: '2%',
			top: '5%',
			width: "96%",
			height: "85%"
		},
		fontSize: 8,
		legend: {
			position: 'top'
		}
	},
	pieData: [
		[ 'Option',		'Value' ],
		[ 'Found 1 +10%',  	134		],
		[ 'Found 2 +12%',  	343		],
		[ 'Found 3 -3%',  	1304	]

	],
	pieOptions: {
		pieHole: 0.4,
		pieSliceText: 'label',
		tooltip: {},
		slices: {
			0: { color: '#157700' },
			1: { color: '#2bde08' },
			2: { color: '#145d03' },
			3: { color: '#289c12' },
			4: { color: '#1c8807' },
			5: { color: '#16580a' },
			6: { color: '#73d661' }
		},
		legend: {
			position: 'none'
		},
		chartArea: {
			left: '0',
			top: '4%',
			width: "100%",
			height: "92%"
		},
		fontSize: 9,
		backgroundColor: 'transparent'
	}
};